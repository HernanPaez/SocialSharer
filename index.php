<?php
$pgTitle=$_GET['title'];
$pgDesc=$_GET['description'];
$pgKeywords=$_GET['keywords'];
$pgAuthor=$_GET['author'];
$pgType=$_GET['type'];
$pgCopyright=$_GET['copyright'];
$pgAppName=$_GET['app_name'];
$pgUrl=$_GET['url'];
$pgTwitterCard=$_GET['twitter_card'];
$pgRedirectURL=$_GET['redirect'];
$pgImage=$_GET['image'];//"https://k44.kn3.net/taringa/3/6/1/3/9/7/3/one1_love/F2B.jpg?1439";

if (empty($pgType)) {
	$pgType = "article";
}
if (empty($pgTwitterCard)) {
	$pgTwitterCard="summary";
}
 
?>

<html>

<!-- for Google -->
<meta name="description" content="<?php echo $pgDesc ?>" />
<meta name="keywords" content="<?php echo $pgKeywords ?>" />

<meta name="author" content="<?php echo $pgAuthor ?>" />
<meta name="copyright" content="<?php echo $pgCopyright ?>" />
<meta name="application-name" content="<?php echo $pgAppName ?>" />

<!-- for Facebook -->          
<meta property="og:title" content="<?php echo $pgTitle ?>" />
<meta property="og:type" content="<?php echo $pgType ?>" />
<meta property="og:image" content="<?php echo $pgImage ?>" />
<meta property="og:url" content="<?php echo $pgUrl ?>" />
<meta property="og:description" content="<?php echo $pgDesc ?>" />

<!-- for Twitter -->          
<meta name="twitter:card" content="<?php echo $pgTwitterCard ?>" />
<meta name="twitter:title" content="<?php echo $pgTitle ?>" />
<meta name="twitter:description" content="<?php echo $pgDesc ?>" />
<meta name="twitter:image" content="<?php echo $pgImage ?>" />

<head>

	<?php
	if ( empty($pgRedirectURL) == false ) {
		echo "<meta charset='UTF-8'>";
		echo "<meta http-equiv='refresh' content='1;url='$pgRedirectURL''>";
		echo "<script type='text/javascript'>";
		echo "window.location.href = '$pgRedirectURL'";
		echo "</script>";
	}	
	?>
	<title><?php echo $pgTitle ?></title>
</head>
<body>
<img src="<?php echo $pgImage ?>" alt="<?php echo $pgTitle ?>">
</body>

</html>